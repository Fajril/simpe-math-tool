﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleMathLib
{
    public class MathLib
    {
        public MathLib()
        {

        }

        const double pi = 3.141592653589793238462643383279502884197;
        const double e = 2.71828182845904523536028747135266249775724709369995;

        public int Factorial(int number)
        {
            if (number < 0)
                throw new DivideByZeroException();
            if (number == 0)
                return 1;
            int result = 1;
            for (int i = 0; i < number; i++)
            {
                result = result * (result + 1);
            }
            return result;
        }
    }
}
